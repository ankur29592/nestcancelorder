package com.alticeusa.cancelSubscription.service;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.alticeusa.cancelSubscription.bo.CancelSubscriptionBO;
import com.alticeusa.cancelSubscription.data.BaseRequestPOJO;
import com.alticeusa.cancelSubscription.data.ResponseInfo;
import com.alticeusa.cancelSubscription.utils.CancelSubscriptionProperties;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.Utils;


@Path("/api/v1/")
public class NestCancelSubscriptionService {
	
	private static final Logger logger = LogManager.getLogger(NestCancelSubscriptionService.class);
	static {
		loadLoggerContextAndProperties();
	}
	
	@Path("/cancelSubscription")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String  JSONResponse (BaseRequestPOJO request , @DefaultValue("") @HeaderParam("Accept") String format){
		logger.info("Invoked API : /nestCancelSubscription/api/v1/cancelSubscription");
		loadAppVersionToLoggerContext();
		String finalString = " ";
		
		CancelSubscriptionBO service = new CancelSubscriptionBO();
		ResponseInfo response = service.nestCancelSubscription(request);
		
		if(format.equalsIgnoreCase("application/xml") && format!= "")
		{
			try{
			String xmlInString = Utils.convertJavaToXml(response);
			finalString = xmlInString;
			}catch(Exception e){
				logger.error("Error while converting java object to xml string ",e);
				}
			
		}
		else {
			
			try{
			finalString = Utils.convertJavaToJson(response);
		}catch(Exception e){
			logger.error("Error while converting java object to json string ",e);
			}
		return finalString;
		}
		return finalString;
		}

	private static void loadAppVersionToLoggerContext() {
		ThreadContext.put("VERSION", Constants.API_VERSION);
	}

	private static void loadLoggerContextAndProperties() {
		loadAppVersionToLoggerContext();
		CancelSubscriptionProperties.getInstance().loadProperties(Constants.API_VERSION);
		logger.info(Constants.API_NAME + " Properties loaded for Version: " + Constants.API_VERSION);
	}
}


package com.alticeusa.cancelSubscription.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "nestSubscriptionResponse")
public class ResponseInfo {
	
	private String footprint="";
	private String accountNumber="";
	private String nestSubscriptionId="";
	private String linkOrderId = "";
	private String sourceApp="";
	private String success="";
	private String errorCode="";
	private String errorMessage="";
	
	public ResponseInfo(){
		
		
	}
	@XmlElement (name="footprint", required = true)
	public String getFootprint() {
		return footprint;
	}
	public void setFootprint(String footprint) {
		this.footprint = footprint;
	}
	@XmlElement (name="accountNumber", required = true)
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@XmlElement (name="nestSubscriptionId", required = true)
	public String getNestSubscriptionId() {
		return nestSubscriptionId;
	}
	public void setNestSubscriptionId(String nestSubscriptionId) {
		this.nestSubscriptionId = nestSubscriptionId;
	}
	@XmlElement (name="linkOrderId", required = true)
	public String getLinkOrderId() {
		return linkOrderId;
	}
	public void setLinkOrderId(String linkOrderId) {
		this.linkOrderId = linkOrderId;
	}
	@XmlElement (name="sourceApp", required = true)
	public String getSourceApp() {
		return sourceApp;
	}
	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}
	@XmlElement (name="success", required = true)
public String getSuccess() {
	return success;
}
public void setSuccess(String success) {
	this.success = success;
}
@XmlElement (name="errorCode", required = true)
public String getErrorCode() {
	return errorCode;
}
public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
}
@XmlElement (name="errorMessage", required = true)
public String getErrorMessage() {
	return errorMessage;
}
public void setErrorMessage(String errorMessage) {
	this.errorMessage = errorMessage;
}
}



package com.alticeusa.cancelSubscription.data;

import org.codehaus.jackson.annotate.JsonProperty;

public class BaseRequestPOJO {
	private String footprint="";
	private String accountNumber="";
	@JsonProperty("nestSubscriptionId")
	private String nestSubscriptionId="";
	private String linkOrderId="";
	private String sourceApp="";

	public BaseRequestPOJO(){
		
	}

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}

	public String getFootprint() {
		return footprint;
	}

	public void setFootprint(String footprint) {
		this.footprint = footprint;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@JsonProperty("nestSubscriptionId")
	public String getNestSubscriptionId() {
		return nestSubscriptionId;
	}

	public void setNestSubscriberId(String nestSubscriptionId) {
		this.nestSubscriptionId = nestSubscriptionId;
	}

	public String getLinkOrderId() {
		return linkOrderId;
	}

	public void setLinkOrderId(String linkOrderId) {
		this.linkOrderId = linkOrderId;
	}
}

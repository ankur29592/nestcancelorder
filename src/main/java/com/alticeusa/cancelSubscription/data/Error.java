package com.alticeusa.cancelSubscription.data;
import java.util.ArrayList;

public class Error {

		String code;
		String message;
		String status;
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		ArrayList<Errors> errors;
		public ArrayList<Errors> getErrors() {
			return errors;
		}
		public void setErrors(ArrayList<Errors> errors) {
			this.errors = errors;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		
}

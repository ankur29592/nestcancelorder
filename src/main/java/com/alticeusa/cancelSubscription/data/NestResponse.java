package com.alticeusa.cancelSubscription.data;

import org.codehaus.jackson.annotate.JsonProperty;

public class NestResponse {

	private String name;
	private int  partnerSubscriptionId;
	private int  partnerCustomerId;
	private String displayName;
	private String productCode;
	private String startTime;
	private String cancellationTime;
	private String status;
	@JsonProperty("associationUrl")
	private String associationUrl;
	private String associationCode;
	Error error;
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPartnerSubscriptionId() {
		return partnerSubscriptionId;
	}
	public void setPartnerSubscriptionId(int partnerSubscriptionId) {
		this.partnerSubscriptionId = partnerSubscriptionId;
	}
	public int getPartnerCustomerId() {
		return partnerCustomerId;
	}
	public void setPartnerCustomerId(int partnerCustomerId) {
		this.partnerCustomerId = partnerCustomerId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getCancellationTime() {
		return cancellationTime;
	}
	public void setCancellationTime(String cancellationTime) {
		this.cancellationTime = cancellationTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssociationURL() {
		return associationUrl;
	}
	public void setAssociationURL(String associationURL) {
		this.associationUrl = associationURL;
	}
	public String getAssociationCode() {
		return associationCode;
	}
	public void setAssociationCode(String associationCode) {
		this.associationCode = associationCode;
	}
	
}

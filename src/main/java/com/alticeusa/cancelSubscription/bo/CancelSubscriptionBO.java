package com.alticeusa.cancelSubscription.bo;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.cancelSubscription.dao.CancelSubscriptionDAO;
import com.alticeusa.cancelSubscription.data.BaseRequestPOJO;
import com.alticeusa.cancelSubscription.data.ResponseInfo;
import com.alticeusa.cancelSubscription.data.SubscriptionDetails;
import com.alticeusa.cancelSubscription.metrics.logging.NestCancelSubscriptionLoggingData;
import com.alticeusa.cancelSubscription.queue.CancelMessageSender;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.Java_JSON_Converter;
import com.alticeusa.cancelSubscription.utils.LogFormatter;
import com.alticeusa.cancelSubscription.utils.Utils;

public class CancelSubscriptionBO {
		String errorStackTrace = null;
		CancelSubscriptionDAO db = new CancelSubscriptionDAO();
		private static final Logger logger = LogManager.getLogger(CancelSubscriptionBO.class);	
		 
 
 public  ResponseInfo nestCancelSubscription(BaseRequestPOJO request){
	 
	 ResponseInfo response = new ResponseInfo();
	 CancelSubscriptionDAO dao = new CancelSubscriptionDAO();
	 CancelMessageSender sender = new CancelMessageSender();
	 SubscriptionDetails details = new SubscriptionDetails();
	 populateResponseWithValuesFromRequest(response,request);
	 long start = System.currentTimeMillis();
	 try{
		 logger.info(">>> nestCancelSubscription() ::: request = {}", LogFormatter.produceJSON(request, Constants.PRETTY_PRINT));
			
		 performMetricsLogging(request, response);
		 
		
		 if(!"".equals(Utils.checkValidRequests(request)) ) {
				
				logger.debug("Check valid request failed.");
				String errMsg = Utils.checkValidRequests(request);
				response = Utils.setBadRequestErrorMessage(response, errMsg);
				performNecessaryLogging(request, response, errorStackTrace, start);
				
				Utils.writeLogForError(request);
				
				return response;
				
			} 
		 else if(!"".equals(Utils.validateRequest(request))) {
				
				logger.debug("Check validate request failed.");
				String errMsg = Utils.validateRequest(request);
				response = Utils.setBadRequestErrorMessage(response, errMsg);
				performNecessaryLogging(request, response, errorStackTrace, start);
				
				Utils.writeLogForError(request);
				
				return response;
				
			}
				
			else{
				 logger.info("Request is valid");
				details = db.checkSubsID(request,response);
				
				if(details.getCode()==0){
					 logger.info("The Nest Subscription ID exists in database");
				if(details.getStatus().equalsIgnoreCase("REQUESTED")){
					logger.info("The Nest Subscription ID is not yet sent to nest");
					int count = dao.updateStatus(details.getSubscriptionID(),details.getOrderID(), "CANCELED");
					if(count == 1){
						
					response.setSuccess("TRUE");
					response.setErrorCode("");
					response.setErrorMessage("");
					logger.info("The Nest Subscription ID is cancelled");	
					performNecessaryLogging(request, response, errorStackTrace, start);
					return response;
					}
				}
				
				else if(details.getStatus().equalsIgnoreCase("NEST_ACTIVE")   || details.getStatus().equalsIgnoreCase("ACTIVE")   || details.getStatus().equalsIgnoreCase("IN_DDP")  || details.getStatus().equalsIgnoreCase("ORDERED") ||details.getStatus().equalsIgnoreCase("CANCEL_PENDING")||details.getStatus().equalsIgnoreCase("ERROR")){
					
					String msg = Java_JSON_Converter.javaToJson(request);
					int count = dao.updateStatus(details.getSubscriptionID(),details.getOrderID(),"CANCEL_PENDING");
					int retry = Integer.parseInt(Constants.THRESHOLD);
					if(count ==1)
					try {
						logger.info("Sending SubscriptionId to queue");
						sender.cancelMessageSender(msg , request.getAccountNumber(),retry);
						response.setSuccess("TRUE");
						response.setErrorCode("");
						response.setErrorMessage("");
						performNecessaryLogging(request, response, errorStackTrace, start);
						return response;
					} catch (Exception e) {
						
						logger.error("Exception occured while publishing nestCancelSubscription request to queue exception:", e);
						response.setSuccess("TRUE");
						response.setErrorCode("");
						response.setErrorMessage("");
						errorStackTrace = ExceptionUtils.getStackTrace(e);
						performNecessaryLogging(request, response, errorStackTrace, start);
					}
				}
				
				else if(details.getStatus().equalsIgnoreCase("CANCEL_SENT") || details.getStatus().equalsIgnoreCase("CANCELED")){
					logger.info("The Nest Subscription ID already cancelled");
					response.setSuccess("FALSE");
					response.setErrorCode("400");
					response.setErrorMessage("Subscription ID already cancelled");
					performNecessaryLogging(request, response, errorStackTrace, start);
					logger.info("The Nest Subscription ID already cancelled");
					Utils.writeLogForError(request);
					return response;
				}}
				else
				{
					 logger.info("The Nest Subscription ID does not exist in database");
					Utils.setNotFoundErrorMessage(response);
					Utils.writeLogForError(request);
					
				}
				performNecessaryLogging(request, response, errorStackTrace, start);
				return  response;
				}
	
	 }catch (Exception ex) {
		Utils.setInternalServerErrorMessage(response);
		logger.error("Exception occured while processing nestCancelSubscription request exception:", ex);
		errorStackTrace = ExceptionUtils.getStackTrace(ex);
		performNecessaryLogging(request, response, errorStackTrace, start);
		Utils.writeLogForError(request);
		return response;
	}
	 }
	
 private void performNecessaryLogging(BaseRequestPOJO request, ResponseInfo response,  String errorStackTrace, long startTime) {
		long endTime = System.currentTimeMillis();
		performApplicationLogging(request, response, startTime, endTime);
		performMetricsLogging(request, response);
		
	}

	private void performApplicationLogging(BaseRequestPOJO request, ResponseInfo response, long startTime, long endTime) {
		long elapsed = endTime - startTime;
		logger.info("<<< nestCancelSubscription() [ Time taken = {} milli-seconds] ::: response = {}", elapsed , LogFormatter.produceJSON(response, Constants.PRETTY_PRINT));
		
	}

 private void populateResponseWithValuesFromRequest (ResponseInfo response, BaseRequestPOJO request) {
		response.setAccountNumber(request.getAccountNumber());
		response.setSourceApp(request.getSourceApp());
		response.setFootprint(request.getFootprint());
		response.setNestSubscriptionId(request.getNestSubscriptionId());
		if(request.getLinkOrderId() == null){
			response.setLinkOrderId("");
		}
		else{		
		response.setLinkOrderId(request.getLinkOrderId());
		}
}
 
 private void performMetricsLogging(BaseRequestPOJO request, ResponseInfo response) {
	 NestCancelSubscriptionLoggingData metricsData = new NestCancelSubscriptionLoggingData(request, response);
		logger.info(metricsData.toString());
	}
}

	
	



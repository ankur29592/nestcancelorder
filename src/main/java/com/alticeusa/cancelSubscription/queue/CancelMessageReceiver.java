package com.alticeusa.cancelSubscription.queue;

import java.util.Properties;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.cancelSubscription.helper.NestCancelSubscriptionSender;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.PropertiesReader;

@WebListener
public class CancelMessageReceiver implements ServletContextListener, MessageListener {
	 	QueueConnection connection =null;
	    ActiveMQConnectionFactory factory;
	    Queue queue = null;
	    MessageConsumer consumer ;
	    Session session = null ;
	    Destination destination ;
	    Context ctx = null;
	    PropertiesReader prop = PropertiesReader.getInstance();
		private static final Logger logger = LogManager.getLogger(CancelMessageReceiver.class);
		
		
		public void onMessage(Message msg) {
		try {
			int retry = Integer.parseInt(Constants.THRESHOLD);
			logger.debug("Received in queue ::: "+ msg);
			NestCancelSubscriptionSender svc = new NestCancelSubscriptionSender();
			String Nest_msg = ((TextMessage) msg).getText();
			svc.nestSendCancel(Nest_msg,retry);
		} catch (JMSException e) {
			logger.error(" Subscription Id "+ msg + " failed to send to NestResponseBO with : ", e);
		}
		
		
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		try {
			if (consumer != null) {
				consumer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
		}catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void contextInitialized(ServletContextEvent arg0) {
		
		try {
			Properties props = new Properties();
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY,Constants.NEST_CANCEL_SUBSCRIPTION_CONTEXT_FACTORY);
			props.setProperty(Context.PROVIDER_URL,Constants.NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL);
			javax.naming.Context ctx = new InitialContext(props);
			factory = (ActiveMQConnectionFactory) ctx.lookup((Constants.CONNECTION_FACTORY));
			connection = factory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = session.createQueue((Constants.NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME));
			consumer = session.createConsumer(queue);
		    consumer.setMessageListener(this);
		    connection.start();
		    logger.debug("initialized");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	}



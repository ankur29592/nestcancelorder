package com.alticeusa.cancelSubscription.queue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.cancelSubscription.dao.CancelSubscriptionDAO;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.Java_JSON_Converter;
import com.alticeusa.cancelSubscription.utils.Utils;


public class CancelMessageSender {
	private static final Logger logger = LogManager.getLogger(CancelMessageSender.class);
	CancelSubscriptionDAO dao1 = new CancelSubscriptionDAO();
	public static CancelMessageSender singleton = null;
	public static CancelMessageSender getInstance() throws Exception  {
		if (singleton == null)
		{
			singleton = new CancelMessageSender();
		}
		return singleton;
	}
	
	Connection connection = null;
	ActiveMQConnectionFactory factory;
	Topic topic;
	MessageProducer producer = null;
	Session session = null;
	Destination destination = null;
	Context ctx = null;
	
	public void contextDestroyed() {
		try {
			if (producer != null) {
				producer.close();
			}
			if (session != null) {
				session.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
	
	public void initializeContext() throws Exception{
		try {
				
			logger.info(">>> initializeContext : initializeContext :  " + Constants.NEST_CANCEL_SUBSCRIPTION_CONTEXT_FACTORY);
			logger.info(">>> Constants.NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL :  " + Constants.NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL);
			logger.info(">>> Constants.CONNECTION_FACTORY :  " + Constants.CONNECTION_FACTORY);
			logger.info(">>> Constants.NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME :  " + Constants.NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME);
			
			
			Properties props = new Properties();
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY, Constants.NEST_CANCEL_SUBSCRIPTION_CONTEXT_FACTORY);
			props.setProperty(Context.PROVIDER_URL, Constants.NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL);
			
			ctx = new InitialContext(props);
			factory = (ActiveMQConnectionFactory) ctx.lookup(Constants.CONNECTION_FACTORY);
			connection = factory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(Constants.NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME);
			producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			connection.start();
			logger.info("context initialized for : " + Constants.NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME);
		} catch (Exception e) {
			logger.error("Exception occured while initialising context");
			e.printStackTrace();
		}

	}
	
	public boolean cancelMessageSender(String json_msg , String accountNo,int retry) {
		logger.info(">>> cancelMessageSender  :  ");
	
		boolean rtnValue = false;
		try {
			logger.info("QueueMsg : json_msg :  " + json_msg);
			initializeContext();
			sendToQueue(json_msg , accountNo);
			rtnValue = true;
			logger.info("<<< cancelMessageSender  :  " + json_msg);
		} catch(Exception ex){
			logger.error("Exception occured while publishing to Queue :", ex);
			String subs = Java_JSON_Converter.JSONStringExtract(json_msg);
			logger.info(">>> Failed to send msg to Queue");
			if(retry > 0){
				logger.info(">>> Retrying sending msg to Queue ");
				cancelMessageSender(json_msg,accountNo,retry-1);
				
			}			
			else
			{ 
				try{
					logger.info(">>> Failed to send msg to Queue. Calling  SP errorResponse() to log error in DB");
					dao1.errorResponse(subs, "ERROR", "Error while publishing into queue" , Integer.parseInt(Constants.THRESHOLD) );
					}
				catch(Exception e1){
					
					logger.info(">>> Failed to log error in DB. Creating a db log file");
					String sdf = new SimpleDateFormat("yyyyMMdd").format(new Date());
					String fileDate = sdf + "/";
					String fileName = accountNo + "SUBSCRIPTION"+subs+ ".txt";
					String directoryName = Constants.LOG_FILE_PATH + fileDate;

					Utils.writeToLogFile(fileName,directoryName, json_msg);
					}
			}
		}
			
	finally{
		contextDestroyed();
		
	}
		return rtnValue;

	}
	
	private void sendToQueue(String jsonMsg , String accNo) throws Exception{
		try{
	
		Message jsonMessage = session.createTextMessage(jsonMsg);
		producer.send(jsonMessage);
		
		}catch(JMSException e){
			logger.error("Exception Occured while sending msg to queue in sendToQueue()");
			
	}}}
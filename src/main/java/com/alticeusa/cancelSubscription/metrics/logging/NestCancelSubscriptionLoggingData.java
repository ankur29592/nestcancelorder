package com.alticeusa.cancelSubscription.metrics.logging;



import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.cancelSubscription.data.BaseRequestPOJO;
import com.alticeusa.cancelSubscription.data.ResponseInfo;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.StringFormatter;
import com.alticeusa.cancelSubscription.utils.Utils;


public class NestCancelSubscriptionLoggingData {

	
	private static final Logger logger = LogManager.getLogger(NestCancelSubscriptionLoggingData.class);	
	
	private final BaseRequestPOJO request;
	private final ResponseInfo response;
	private final String dateTimeStamp;
	
	public NestCancelSubscriptionLoggingData(BaseRequestPOJO request, ResponseInfo response) {
		
		this.request = request;
		this.response = response;
		this.dateTimeStamp = Utils.getCurrentTimeStamp(Constants.METRICS_LOGGING_COMMON_DATE_TIME_FORMAT);
	}
	public String toString() {
		return getLogMetricsJSONString();
	}
	private String getLogMetricsJSONString() {
		String metricsJSON = Constants.EMPTY_JSON;
		try {
			LinkedHashMap<String, Object> metricsKeyVaulePairs = new LinkedHashMap<String, Object>();
			addHeaderKeyValuePairs(metricsKeyVaulePairs);
			addRequestKeyValuePairs(metricsKeyVaulePairs);
			addResponseKeyValuePairs(metricsKeyVaulePairs);
			metricsJSON = MetricsLoggingUtils.buildJSONStringFromMap(metricsKeyVaulePairs);
		} catch (Exception e) {
			logger.error("Exception in nestCancelSubscription Metrics logging:", e.getMessage());
		}
		return metricsJSON;
	}

	private void addHeaderKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_DATE_TIME_KEY, dateTimeStamp);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY, Thread.currentThread().getName());
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_CLASS_NAME_KEY, Constants.METRICS_LOGGING_CLASS_NAME_VALUE);
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_METHOD_NAME_KEY, Constants.METRICS_LOGGING_METHOD_NAME_VALUE);

	}

	private void addRequestKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_REQUEST_ACCOUNTNUMBER_KEY, request.getAccountNumber());
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_SOURCE_APP_KEY, request.getSourceApp());
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_FOOTPRINT_KEY, request.getFootprint());
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_NEST_SUBSCRIPTION_ID_KEY, request.getNestSubscriptionId());
		metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_LINK_ORDER_ID_KEY, request.getLinkOrderId());
	}
	
	public void addResponseKeyValuePairs(LinkedHashMap<String, Object> metricsKeyVaulePairs) {
		if (areResponseFieldsPresent()) {
			
			metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_SUCCESS_KEY, response.getSuccess());
			metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_ERROR_CODE_KEY, response.getErrorCode());
			metricsKeyVaulePairs.put(Constants.METRICS_LOGGING_COMMON_ERROR_MESSAGE_KEY, response.getErrorMessage());
		}
	}
	
	private boolean areResponseFieldsPresent() {
	    if(StringFormatter.checkNullString(response.getErrorCode(), response.getErrorCode())){
	    	return false;
	    } else {
	    	return true;
	    }
	}

}

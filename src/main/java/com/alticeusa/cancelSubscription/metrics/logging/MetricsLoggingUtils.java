package com.alticeusa.cancelSubscription.metrics.logging;
import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

public class MetricsLoggingUtils {
private static final Logger logger = LogManager.getLogger("MetricsLoggingUtils.class");
	
	public static String buildJSONStringFromMap(LinkedHashMap<String, Object> metricsKeyValuePairs){
		
		try{
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(metricsKeyValuePairs);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(" Error in Metrics Logging " +e.getMessage());
			throw new RuntimeException(e);
		}
	}

}

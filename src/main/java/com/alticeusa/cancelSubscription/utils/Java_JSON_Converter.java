package com.alticeusa.cancelSubscription.utils;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

public class Java_JSON_Converter {
	 public static String javaToJson(Object o) {
         String jsonString = null;
         try {
             ObjectMapper objectMapper = new ObjectMapper();
             jsonString = objectMapper.writeValueAsString(o);
             JSONObject json = new JSONObject(jsonString);
             int msg = json.getInt("nestSubscriptionId");
             return "{\"nestSubscriptionId\":"+"\""+ msg + "\"}" ;

         } catch (JsonGenerationException e) {
             
         } catch (JsonMappingException e) {
           
         } catch (IOException e) {
            
         }
         return jsonString;
     }
	public static String JSONStringExtract(String jsonMsg){
		JSONObject json = new JSONObject(jsonMsg);
		String subs = json.getString("nestSubscriptionId");
		
		return subs;
	}
}

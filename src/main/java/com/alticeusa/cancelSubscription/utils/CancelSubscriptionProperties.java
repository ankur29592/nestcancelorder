package com.alticeusa.cancelSubscription.utils;

import java.util.Properties;


public class CancelSubscriptionProperties {
	public Properties prop = null;
	public static CancelSubscriptionProperties singleton = null;
	
	public CancelSubscriptionProperties()	{
		loadProperties(Constants.API_NAME);
	}
	
	public static CancelSubscriptionProperties getInstance()
	{
		if (singleton == null)
		{
			singleton = new CancelSubscriptionProperties();
		}//if
		return singleton;
	}//getInstance
	
	public String getValue(String key) 
	{
		return prop.getProperty(key);
	}// getValue
	
	public Properties loadProperties(String apiVersion)
	{
		prop = PropertiesReader.getInstance().getParams();
		return prop;
	}//loadProperties		
}

package com.alticeusa.cancelSubscription.utils;

public class Constants {

	public static PropertiesReader prop = PropertiesReader.getInstance();

	
	public static final String ERR_200="error.200";
	public static final String ERR_400="error.400";
	public static final String ERR_500="error.500";
	public static final String ERR_404="error.404";
	public static boolean PRETTY_PRINT = StringFormatter.toBoolean(prop.getValue("logJSONPrettyPrint"));

	public static final String NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL = prop.getValue("NEST_CANCEL_SUBSCRIPTION_MESSAGE_URL");
	public static final String NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME = prop.getValue("NEST_CANCEL_SUBSCRIPTION_QUEUE_NAME");
	public static final String CONNECTION_FACTORY = prop.getValue("CONNECTION_FACTORY");
	public static final String NEST_CANCEL_SUBSCRIPTION_CONTEXT_FACTORY = prop.getValue("NEST_CANCEL_SUBSCRIPTION_CONTEXT_FACTORY");
	public static final String LOG_FILE_PATH = prop.getValue("LOG_FILE_PATH");
	
	public static final String DB_DRIVER= prop.getValue("DB_DRIVER");
	public static final String DB_CONNECTION= prop.getValue("DB_CONNECTION");
	public static final String DB_USER = prop.getValue("DB_USER");
	public static final String DB_PASSWORD= prop.getValue("DB_PASSWORD");
	
	public static final String THRESHOLD = prop.getValue("THRESHOLD");
	public static final String FILE_PATH = prop.getValue("LOG_FILE_PATH");
	
	public static final String API_VERSION = prop.getValue("API_VERSION");
	
	public static final String API_NAME= prop.getValue("API_NAME");
	
	public static final String METRICS_LOGGING_COMMON_FOOTPRINT_KEY = "footprint";
	public static final String METRICS_LOGGING_COMMON_NEST_SUBSCRIPTION_ID_KEY = "nestSubscriptonId";
	public static final String METRICS_LOGGING_COMMON_LINK_ORDER_ID_KEY = "linkOrderId";
	public static final String METRICS_LOGGING_REQUEST_ACCOUNTNUMBER_KEY = "accountNumber";
	public static final String METRICS_LOGGING_COMMON_SOURCE_APP_KEY = "sourceApp";
	
	public static final String METRICS_LOGGING_COMMON_SUCCESS_KEY = "success";
	public static final String METRICS_LOGGING_COMMON_ERROR_CODE_KEY = "errorCode";
	public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE_KEY = "errorMessage";

	public static final String EMPTY_JSON = "{}";
	
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_KEY = "Datetime";
	//Example format : 2016-04-01T14:02:02.333
	public static final String METRICS_LOGGING_COMMON_DATE_TIME_FORMAT = "YYYY-MM-dd'T'HH:mm:ss.SSS";
	public static final String METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY = "Thread";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY = "ContextName";
	public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE = "Nest-Provisioning";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY = "SubContextName";

	public static final String METRICS_LOGGING_COMMON_CLASS_NAME_KEY = "ClassName";
	public static final String METRICS_LOGGING_COMMON_METHOD_NAME_KEY = "MethodName";
	
	public static final String METRICS_LOGGING_CLASS_NAME_VALUE = "com.alticeusa.cancelSubscription.NestCancelSubscriptionService";
	public static final String METRICS_LOGGING_METHOD_NAME_VALUE = "nestCancelSubscription";
	public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE = "nestCancelSubscription";

	



}

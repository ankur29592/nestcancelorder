package com.alticeusa.cancelSubscription.utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.alticeusa.cancelSubscription.data.BaseRequestPOJO;
import com.alticeusa.cancelSubscription.data.ResponseInfo;


public class Utils {
	
	private static CancelSubscriptionProperties prop = CancelSubscriptionProperties.getInstance();
	private static final Logger logger = LogManager.getLogger(Utils.class);
	public static String convertJavaToXml(ResponseInfo o) throws JAXBException {
		StringWriter xmlString = new StringWriter();
		JAXBContext contextObj = JAXBContext.newInstance(ResponseInfo.class);  
	    Marshaller marshaller = contextObj.createMarshaller();  
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshaller.marshal(o, xmlString);
	    String xml = xmlString.toString();
		return xml;
	}
	public static String convertJavaToJson(Object o) throws JsonGenerationException, JsonMappingException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(o);
		return jsonInString;
	}
	public static String getCurrentTimeStamp(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date now = new Date();
		String formattedDateTime = dateFormat.format(now);
		return formattedDateTime;
	}

	public static ResponseInfo setInternalServerErrorMessage(ResponseInfo response) {
		setSuccessResponseField(false, response);
		getErrorCodeInfo(Constants.ERR_500, response);
		return response;
	}

	
	public static ResponseInfo setBadRequestErrorMessage(ResponseInfo response , String errorMsg) {
		setSuccessResponseField(false, response);
		response.setErrorCode("400");
		response.setErrorMessage(errorMsg);
		return response;
	}

	public static void setOKStatusMessage(ResponseInfo response) {
		setSuccessResponseField(true, response);
		getErrorCodeInfo(Constants.ERR_200, response);
	}
	
	public static void setNotFoundErrorMessage(ResponseInfo response) {
		setSuccessResponseField(false, response);
		getErrorCodeInfo(Constants.ERR_404, response);
	}
	
	public static void getErrorCodeInfo(String lookup, ResponseInfo responsePojo)
	{
		String [] returnArray = new String[2];
		returnArray = prop.getValue(lookup).split("\\|");
		responsePojo.setErrorCode(returnArray[0]); 
		responsePojo.setErrorMessage(returnArray[1]);	
	}
	
	private static void setSuccessResponseField(boolean isSuccess, ResponseInfo response) {
		String success = Boolean.toString(isSuccess);
		response.setSuccess(success);
	}
	

	public static String isSourceAppVaild(String sourceApp) {
		sourceApp = sourceApp.trim();
		String errorMsg = "";
		
		if(!("IDA".equalsIgnoreCase(sourceApp) || "ECOMMERCE".equalsIgnoreCase(sourceApp))) {
			errorMsg = "Invalid value for sourceApp ";	
			return errorMsg;
		}
		return errorMsg;
	}
	
	public static String isFootPrintVaild(String footPrint) {
		footPrint = footPrint.trim();
		String errorMsg = "";
		
		if(!("OPT".equalsIgnoreCase(footPrint) || "SDL".equalsIgnoreCase(footPrint))) {
			errorMsg = "Invalid value for footPrint ";	
			return errorMsg;
		}
		return errorMsg;
	}
	private static boolean isNumber(String input) {
		for(char c : input.toCharArray()) {
			if(!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}
	public static String isNestSubscriptionIdValid(String nestSubscriptionId) {
	
		String errorMsg = "";
		
		if(!(isNumber(nestSubscriptionId))) {
			errorMsg = "Invalid value for nestSubscriptionId ";	
			return errorMsg;
		}
		return errorMsg;
	}
	 public static String validateRequest(BaseRequestPOJO request) {
			StringBuffer errorMsg = new StringBuffer();
			errorMsg.append("");
			if(!("".equals(isSourceAppVaild(request.getSourceApp())))) {
				errorMsg.append(isSourceAppVaild(request.getSourceApp()));
			}
			
			if(!("".equals(isFootPrintVaild(request.getFootprint())))) {
				errorMsg.append(isFootPrintVaild(request.getFootprint()));
			}
			if(!("".equals(isNestSubscriptionIdValid(request.getNestSubscriptionId())))){
				errorMsg.append(isNestSubscriptionIdValid(request.getNestSubscriptionId()));
			}
			return errorMsg.toString();
			}
	 
	 public static String checkValidRequests(BaseRequestPOJO request) {
			StringBuffer errorMsg = new StringBuffer();
			errorMsg.append("");
			if(StringFormatter.nullOrBlank(request.getSourceApp()) ) {
				errorMsg.append("sourceApp is missing ");
				logger.info(" <<< checkValidRequests sourceApp is missing");
			}
			if(StringFormatter.nullOrBlank(request.getFootprint()) ) {
				errorMsg.append("footPrint is missing ");
				logger.info(" <<< checkValidRequests footPrint is missing");
			}
			
			if(StringFormatter.nullOrBlank(request.getAccountNumber()) ) {
				errorMsg.append("accountNumber is missing ");
				logger.info(" <<< checkValidRequests accountNumber is missing");
			}
			
			if(StringFormatter.nullOrBlank(request.getNestSubscriptionId()) ) {
				errorMsg.append("nestSubscriptionId is missing ");
				logger.info(" <<< checkValidRequests nestSubscriptionId is missing");
			}		
			return errorMsg.toString();
		} 
 
	 public static void writeLogForError(BaseRequestPOJO request){
			logger.debug("inside writeLogForError() creating log file for Error response");
			try {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String fileDate = sdf.format(date) + "/";
		
			 String directoryName = Constants.LOG_FILE_PATH + fileDate;
			 logger.debug("inside writeLogForError() creating directory name for Error response" + directoryName);
			 if(!(request.getAccountNumber().isEmpty())){
				 logger.debug("No AccountNumber found for request...Not writing to log File");
			String fileName = request.getAccountNumber()+ "_cancelSubscription.txt";
			logger.debug("inside writeLogForError() creating file name for Error response" + fileName);
			ObjectMapper mapper = new ObjectMapper();
			String jsonLog ;
			jsonLog = mapper.writeValueAsString(request);
			Utils.writeToLogFile(fileName,directoryName,jsonLog);
			 }} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	 public static void writeToLogFile(String fileName,String directoryName,String msg) {
		 logger.info("Writing to log file:");
			File directory = new File(directoryName);
		    if (! directory.exists()){
		    	logger.info("creating directory log file:");
		        directory.mkdir();
		        // If you require it to make the entire directory path including parents,
		        // use directory.mkdirs(); here instead.
		    }
		    FileWriter fw =null;
		    BufferedWriter bw =null;
		    try{
		    	logger.info("creating File file:");
		    	 File file = new File(directoryName + "/" + fileName);
		    	 logger.debug("file absolute path " + file.getAbsoluteFile());
		 	    fw = new FileWriter(file.getAbsoluteFile(),true);
		        bw = new BufferedWriter(fw);
		        bw.write(msg);
		        bw.newLine();
		        bw.close();
		    
			} catch (IOException e) {
				logger.error("Error Occured while Writing to log file:",e);
			} finally {
				try {
					if (bw != null)
						bw.close();
					if (fw != null)
						fw.close();
				} catch (IOException ex) {

					ex.printStackTrace();
				}
			}
		}
	 }

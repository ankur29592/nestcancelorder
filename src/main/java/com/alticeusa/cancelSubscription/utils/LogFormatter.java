package com.alticeusa.cancelSubscription.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;;

public final class LogFormatter
{
	private static final Logger logger = LogManager.getLogger(LogFormatter.class);

    public static String produceJSON(Object obj, boolean prettyPrint)
    {
    	logger.info(">>> produceJSON");
    	ObjectMapper mapper = new ObjectMapper();
    	String json= "";
        try
        {
        	json = prettyPrint ? mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj):(mapper.writeValueAsString(obj));
           
    	}
        catch(Exception ex)
        {
    		logger.error("[Exception] @produceJSON : {}",ex.getMessage(), ex);
    	}
        logger.info("<<< produceJSON");
        return json;
    }

    
      
}
package com.alticeusa.cancelSubscription.utils;

import java.text.MessageFormat;

public class StringFormatter {

	
	/**
	 * This method returns a boolean value as true if the input string is empty or blank.
	 * @param inputString
	 * @return boolean value true/false
	 */
	public static boolean nullOrBlank(String inputString)
	{
		return ( inputString == null ||
				( inputString != null && inputString.trim().equalsIgnoreCase(""))|| 
				(inputString != null && inputString.trim().length() == 0)||
				( inputString != null && inputString.trim().equalsIgnoreCase("null")));
		
	}
	
	public static boolean checkNullString(String... inputArray) 
	{
		for (String inputString : inputArray) 
		{
			if(nullOrBlank(inputString))
			{
				return true;
			}
		}
		return false;
	}
	/**
	 * This method will format a dynamic string and put in the values passed in 
	 * <code>args</code> in the placeholders of the <code>msg</code>
	 * @param msg
	 * @param args
	 * @return
	 */
	public static String formatString(String msg, Object... args )
	{
    	return MessageFormat.format(msg, args);
    }//formatString
	public static boolean isNumeric(String inputString)
    {
    	if (nullOrBlank(inputString))
    	{
    		return false;
    	}//if
    	else
    	{	
	        for (char c : inputString.toCharArray())
	        {
	            if (!Character.isDigit(c)) return false;
	        }
    	}//else
        return true;
    }
	public static boolean toBoolean(String inputString)
    {
        return (inputString != null && (inputString.equalsIgnoreCase("Y") || 
        		inputString.equalsIgnoreCase("TRUE") ||
        		inputString.equalsIgnoreCase("T")||
        		inputString.equalsIgnoreCase("SUCCESS")||
        		inputString.equalsIgnoreCase("S")||
        		inputString.equalsIgnoreCase("YES")));
    }//toBoolean
	
}

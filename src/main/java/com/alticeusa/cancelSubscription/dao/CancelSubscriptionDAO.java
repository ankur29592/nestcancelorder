package com.alticeusa.cancelSubscription.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.cancelSubscription.data.BaseRequestPOJO;
import com.alticeusa.cancelSubscription.data.NestResponse;
import com.alticeusa.cancelSubscription.data.ResponseInfo;
import com.alticeusa.cancelSubscription.data.SubscriptionDetails;
import com.alticeusa.cancelSubscription.utils.Utils;

public class CancelSubscriptionDAO  extends BaseDAO implements DBInterface {
	static Connection con = null;
	static CallableStatement  csmt = null;
	private static final Logger logger = LogManager.getLogger(CancelSubscriptionDAO.class);
		
	//private static PropertiesReader prop = PropertiesReader.getInstance();
	
	private final String NEST_DS = "jdbc/NEST_DS";
	
	public static CancelSubscriptionDAO instance = null;
	public static CancelSubscriptionDAO getInstance() throws Exception  {
		if (instance == null)
		{
			instance = new CancelSubscriptionDAO();
		}
		return instance;
	}
	
	public Connection getDBConnection() throws ClassNotFoundException, SQLException {
		Connection dbConnection = null;
		
		try {
			dbConnection = getConnection(NEST_DS);
		} catch (Exception e) {
			if(e.getMessage()!=null)
				logger.debug("Error creating connection from DataSource :"+NEST_DS+" Exception :"+e.getMessage());
			logger.debug("Error creating connection from DataSource :"+NEST_DS);
		}
		return dbConnection;
	}


	
	
		public SubscriptionDetails checkSubsID(BaseRequestPOJO req, ResponseInfo response)
		{
			
			logger.info(">>>>> In DAO checkSubsID:"+ req.getNestSubscriptionId());
			SubscriptionDetails details = new SubscriptionDetails();
			try {
			
			con = getDBConnection();
			csmt = con.prepareCall(CANCEL_ORDER_SUBSCRIPTION);
			csmt.setString(1,req.getAccountNumber());
			csmt.setInt(2,(Integer.parseInt(req.getNestSubscriptionId())));
			csmt.registerOutParameter(3, java.sql.Types.INTEGER);
			csmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			csmt.registerOutParameter(5, java.sql.Types.INTEGER);
			csmt.registerOutParameter(6, java.sql.Types.INTEGER);
			csmt.registerOutParameter(7, java.sql.Types.INTEGER);
			csmt.registerOutParameter(8, java.sql.Types.VARCHAR);
			csmt.executeUpdate();
			logger.info(">>>>> checkSubsID.execute()");
			details.setStatus(csmt.getString(4));
			details.setCount(csmt.getInt(3));
			details.setOrderID(csmt.getInt(5));
			details.setSubscriptionID(csmt.getInt(6));
			details.setCode(csmt.getInt(7));
			details.setErrorMsg(csmt.getString(8));
			
			logger.info(">>>>> checkSubsID <"+"> code <"+details.getCode()+ "> message <"+details.getErrorMsg()+ ">"+ "> OrderID <"+details.getOrderID()+ ">"+ "> SubscriptionID <"+details.getSubscriptionID()+ ">"+ "> Status <"+details.getStatus()+ ">");
			
			return details;
					
		} catch (SQLException e) {
			logger.error("Exception occured while calling SP checkSubsID():", e);
			Utils.setInternalServerErrorMessage(response);
			Utils.writeLogForError(req);
			return details;
		
		} catch (ClassNotFoundException e) {
		
			
			logger.error("Exception occured while calling SP checkSubsID():", e.getMessage());
			return details;
		}
			finally
			{
				if(con!= null )
				{
					try {
						if(csmt!=null)
						csmt.close();
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				}
			}
		
	}

			public int updateStatus(int subscriptionID, int orderID, String status) {	
				
				logger.info(">>>>> In DAO updateStatus:");
				try {
					
					con = getDBConnection();
									
					csmt = con.prepareCall(UPDT_ORDER_SUBSCRIPTION_STATUS);
									
					csmt.setInt(1,subscriptionID);
					csmt.setInt(2, orderID);
					csmt.setString(3,status);
					csmt.registerOutParameter(4, java.sql.Types.INTEGER);
					csmt.registerOutParameter(5, java.sql.Types.INTEGER);
					csmt.registerOutParameter(6, java.sql.Types.VARCHAR);
					csmt.executeUpdate();
					logger.info(">>>>> updateStatus.execute()");
					int count = csmt.getInt(4);
					int code = csmt.getInt(5);
					String errormsg = csmt.getString(6);
					logger.info(">>>>> updateStatus <"+"> code <"+code+ "> ErrorMessage <"+errormsg+ ">"+ "> count <"+count+ ">");
					return count;
					
				} catch (SQLException e) {
					logger.error("Exception occured while calling SP updateStatus():", e);
				} catch (ClassNotFoundException e) {
					logger.error("Exception occured while calling SP updateStatus():", e.getMessage());

				}
				finally
				{
					logger.info("Closing connection");
					if(con!= null )
					{
						try {
							if(csmt!=null)
							csmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						
					}
				}
				logger.info("<<< out DAO CancelSubscriptionDAO for updateStatus :");	
				return 0;
			}
	
			public void sentSucessfull(NestResponse resp , int nestSubscriptionId) {
				logger.info(">>>>> In DAO sentSucessfull:");
				try{
								
				con = getDBConnection();
				csmt = con.prepareCall(UPDATE_ORDER_SUBSCRIPTION_CANCEL);
				csmt.setInt(1,resp.getPartnerCustomerId());
				csmt.setInt(2,resp.getPartnerSubscriptionId());
				csmt.setInt(3,nestSubscriptionId);
				csmt.setString(4, resp.getStatus());
				csmt.setString(5, resp.getCancellationTime());
				csmt.registerOutParameter(6, java.sql.Types.INTEGER);
				csmt.registerOutParameter(7, java.sql.Types.INTEGER);
				csmt.registerOutParameter(8, java.sql.Types.VARCHAR);
				csmt.executeUpdate();
				logger.info(">>>>> sentSucessfull.execute()");
				int count = csmt.getInt(6);
				int code = csmt.getInt(7);
				String errormsg = csmt.getString(8);
				
				logger.info(">>>>> updateStatus <"+"> code <"+code+ "> ErrorMessage <"+errormsg+ ">"+ "> count <"+count+ ">");
				} catch (SQLException e) {
					logger.error("Exception occured while calling SP sentSucessfull():", e);
			} catch (ClassNotFoundException e) {
				logger.error("Exception occured while calling SP sentSucessfull():", e);
				}
				finally
				{
					if(con!= null )
					{
						try {
							if(csmt!=null)
							csmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						
					}
				}
				logger.info("<<< out DAO CancelSubscriptionDAO for sentSucessfull :");	
				}
			
			public void errorResponse(String nestSubscriptionId , String errorStatus, String errorMsg, int errorCount ){
				logger.info(">>>>> In DAO errorResponse:");
				try {
					con = getDBConnection();
					csmt = con.prepareCall(SET_ERROR_SUBSCRIPTION);
					csmt.setString(1,nestSubscriptionId);
					csmt.setString(2, errorStatus);
					csmt.setString(3,errorMsg);
					csmt.setInt(4,errorCount);
					csmt.registerOutParameter(5, java.sql.Types.INTEGER);
					csmt.registerOutParameter(6, java.sql.Types.VARCHAR);
					
					csmt.executeUpdate();
					logger.info(">>>>> sentSucessfull.execute()");
					int code = csmt.getInt(5);
					String errormsg = csmt.getString(6);
					logger.info(">>>>> updateStatus <"+"> code <"+code+ "> ErrorMessage <"+errormsg+ ">");
					
				} catch (SQLException e) {
					logger.error("Exception occured while calling SP errorResponse():", e);
					
				} catch (ClassNotFoundException e) {
					logger.error("Exception occured while calling SP errorResponse():", e);
				}
				finally
				{
					if(con!= null )
					{
						try {
							if(csmt!=null)
							csmt.close();
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						
					}
				}
				logger.info("<<< out DAO CancelSubscriptionDAO for errorResponse :");	
			}
			
	}
		
	

package com.alticeusa.cancelSubscription.dao;

public interface DBInterface {

	public static final String UPDT_ORDER_SUBSCRIPTION_STATUS = "{call google_nest.update_subscription_status(?,?,?,?,?,?)}";
	public static final String CANCEL_ORDER_SUBSCRIPTION = "{call google_nest.cancel_order_subscription(?,?,?,?,?,?,?,?)}";
	public static final String SET_ERROR_SUBSCRIPTION = "{call google_nest.set_error_subscription(?,?,?,?,?,?)}";
	public static final String UPDATE_ORDER_SUBSCRIPTION_CANCEL = "{call google_nest.update_order_subscribe_cancel(?,?,?,?,?,?,?,?)}";
	
	}



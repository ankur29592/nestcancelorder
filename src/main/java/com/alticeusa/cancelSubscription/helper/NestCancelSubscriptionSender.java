package com.alticeusa.cancelSubscription.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.alticeusa.cancelSubscription.dao.CancelSubscriptionDAO;
import com.alticeusa.cancelSubscription.data.NestResponse;
import com.alticeusa.cancelSubscription.utils.Constants;
import com.alticeusa.cancelSubscription.utils.Java_JSON_Converter;
import com.alticeusa.cancelSubscription.utils.Utils;
import com.alticeusa.nest.common.auth.NestSubscriptionClient;
import com.alticeusa.nest.common.base.GenericResponse;


public class NestCancelSubscriptionSender {
	
	private static final Logger logger = LogManager.getLogger(NestCancelSubscriptionSender.class);
	long start = System.currentTimeMillis();
	public void nestSendCancel(String subscriptionID , int retry){	
	
	logger.info(">>> nestSendCancel()");	
	CancelSubscriptionDAO dao = new CancelSubscriptionDAO();
	GenericResponse nestResponse;


	String subs = Java_JSON_Converter.JSONStringExtract(subscriptionID);
	int sub = Integer.parseInt(subs);
	
	try {
	NestSubscriptionClient client = new NestSubscriptionClient();
	nestResponse = client.cancelNestPartnerSubscription(subs);
	logger.info("NestResponse: "+ nestResponse.toString());
	ObjectMapper mapper = new ObjectMapper();
	NestResponse response = mapper.readValue(nestResponse.getResponseBody(), NestResponse.class);
	
	if(nestResponse.getResponseCode() == 200 ){
		logger.info(">>> Successfull response from Nest");
	if(response.getStatus().equalsIgnoreCase("CANCELED")){
		logger.info(">>> sending response from Nest to database");
		dao.sentSucessfull(response , sub);
		
		}
	else if (!response.getStatus().equalsIgnoreCase("CANCELED")){
		logger.info(">>> Request sent to Nest");
	dao.updateStatus(response.getPartnerCustomerId(),sub ,"CANCEL_SENT");
		}
}
	
else if(nestResponse.getResponseCode() == 400 || nestResponse.getResponseCode() == 404){
	
	logger.info(">>> Error Response from Nest");
	logger.info(">>>>> ErrorResponse <"+"> code <"+response.getError().getCode()+ "> ErrorMessage <"+response.getError().getMessage()+ ">");

//		dao.errorResponse(subs,"ERROR",response.getError().getCode().concat(response.getError().getMessage()), 1);
//		
	}

	}
catch (Exception e1) {
	logger.info(">>> Failed to send request to Nest");
	if(e1.getMessage()!=null){
		logger.info(">>> Exception msg :"+e1.getMessage());
	}
	if(retry>0){
		logger.info(">>> Retrying sending request to Nest ");
		nestSendCancel(subscriptionID, retry-1);
}
		else
		{ 
			try{
				logger.info(">>> Failed to send request to Nest. Calling  SP errorResponse() to log error in DB");
				dao.errorResponse(subs, "ERROR", "Error while sending request to Nest" , Integer.parseInt(Constants.THRESHOLD) );
				}
			catch(Exception e){
				logger.info(">>> Failed to log error in DB. Creating a db log file");
			
				String sdf = new SimpleDateFormat("yyyyMMdd").format(new Date());
				String fileDate = sdf + "/";
				String fileName ="SUBSCRIPTION"+subs+ ".txt";
				String directoryName = Constants.LOG_FILE_PATH + fileDate;
				Utils.writeToLogFile(fileName,directoryName, subscriptionID);
				
				}
		}
		}
	
	
}}
